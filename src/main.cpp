/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com
*********/

#include <DallasTemperature.h>
#include <OneWire.h>
#include <WiFi.h>
#include <Wire.h>

// degree sign
const String degree("\xB0");

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);
DeviceAddress roofTempSensor = {0x28, 0xF8, 0x19, 0xF8, 0x0C, 0x00, 0x00, 0x94};
DeviceAddress poolTempSensor = {0x28, 0x60, 0xFF, 0x8F, 0x62, 0x20, 0x01, 0x22};
unsigned long lastAutoRelayToggle = 0;
unsigned long lastOpenCloseMovement = millis();
bool firstToggle = true;
bool manualOverride = false;

// bool alreadyScanned = false;
const char *ssid = "home";
const char *password = ".,-ramalamad1ngd0ng.,-";

WiFiServer webServer(80);
String header;
float tempGoal = 26.0;
const int relayControlPinOpen = 25;
const int relayControlPinClose = 26;
String heatingState = "aus";
enum Energy
{
  running,
  cut
};
Energy energyState;
unsigned long currentTime = millis();
unsigned long previousTime = 0;
const int timeoutTime = 2000;

void scanForWifiNetworks();
void initWiFi();
void readTemps(float &roofTemp, float &poolTemp);
void buildHttpResponse(WiFiClient &webClient, const float &roofTemp,
                       const float &poolTemp);
void handleWebView(float roofTemp, float poolTemp);
void openValve();
void closeValve();
void cutEnergy();
void setup()
{
  // Start the Serial Monitor
  Serial.begin(115200);
  // Start the DS18B20 temperature sensor
  // while (!Serial)
  // {
  // }
  sensors.begin();
  Serial.print("Found ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" devices on bus.");
  initWiFi();
  webServer.begin();
  pinMode(relayControlPinOpen, OUTPUT);
  pinMode(relayControlPinClose, OUTPUT);
  cutEnergy();
  lastOpenCloseMovement = millis();
}

void loop()
{
  if (millis() < lastAutoRelayToggle || millis() < lastOpenCloseMovement)
  {
    lastAutoRelayToggle = millis();
    lastOpenCloseMovement = millis();
  }

  float roofTemp;
  float poolTemp;
  readTemps(roofTemp, poolTemp);

  // Display the temperatures on serial
  Serial.print(roofTemp);
  Serial.println(" " + degree + "C");
  Serial.print(poolTemp);
  Serial.println(" " + degree + "C");

  if (millis() - lastOpenCloseMovement > 60000 && energyState == running)
  {
    Serial.println("Cutting energy after 60s timeout");
    cutEnergy();
  }

  // only control automatically if manual override is inactive and toggle only every 10 minutes, if it is not the first toggle
  if (!manualOverride && (millis() - lastAutoRelayToggle > 600000 || firstToggle))
  {
    // only open relay automatically if 
    if (roofTemp - poolTemp > 5 && // rooftemp is 5°C warmer than pool
        roofTemp > 26.0 &&         // rooftemp is above 26°C
        roofTemp > tempGoal &&     // rooftemp is above temperature goal
        poolTemp <= tempGoal)      // and goal temperature is not yet reached
    {
      Serial.println("auto open");
      openValve();
      lastAutoRelayToggle = millis();
      firstToggle = false;
    }
    else
    {
      Serial.println("auto close");
      closeValve();
      lastAutoRelayToggle = millis();
      firstToggle = false;
    }
  }

  // try to auto reconnect on lost wifi connection
  if (!WiFi.isConnected())
  {
    WiFi.reconnect();
    delay(10000);
  }

  handleWebView(roofTemp, poolTemp);
}
void handleWebView(float roofTemp, float poolTemp)
{
  WiFiClient webClient = webServer.available();
  if (webClient)
  {
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client");
    String currentLine = "";
    while (webClient.connected() && currentTime - previousTime <= timeoutTime)
    {
      currentTime = millis();
      if (webClient.available())
      {
        char c = webClient.read();
        Serial.write(c);
        header += c;
        if (c == '\n')
        {
          // send response on 2 consecutive newline chars b/c this is end of
          // HTTP request
          if (currentLine.isEmpty())
          {
            webClient.println("HTTP/1.1 200 OK");
            webClient.println("Content-type:text/html");
            webClient.println("Connection: close");
            webClient.println();

            if (header.indexOf("GET /toggleManualOverride/on") >= 0)
            {
              Serial.println("Manual Override On");
              manualOverride = true;
            }
            if (header.indexOf("GET /toggleManualOverride/off") >= 0)
            {
              Serial.println("Automatic control active");
              manualOverride = false;
            }
            if (header.indexOf("GET /relayManual/on") >= 0)
            {
              Serial.println("Relay set to on");
              openValve();
            }
            else if (header.indexOf("GET /relayManual/off") >= 0)
            {
              Serial.println("Relay set to off");
              closeValve();
            }
            else if (header.indexOf("GET /tempGoal/inc") >= 0)
            {
              tempGoal += 0.5;
              Serial.println("Increasing temp goal to " + String(tempGoal, 1));
            }
            else if (header.indexOf("GET /tempGoal/dec") >= 0)
            {
              tempGoal -= 0.5;
              Serial.println("Decreasing temp goal to " + String(tempGoal, 1));
            }
            buildHttpResponse(webClient, roofTemp, poolTemp);

            // Break out of the while loop
            break;
          }
          else
          { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        }
        else if (c != '\r')
        {                   // if you got anything else but a carriage
                            // return character,
          currentLine += c; // add it to the end of the currentLine
        }
      }
    }
    header = "";
    webClient.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
void buildHttpResponse(WiFiClient &webClient, const float &roofTemp,
                       const float &poolTemp)
{ // Display the HTML web page
  webClient.println("<!DOCTYPE html><html>");
  webClient.println("<head><meta name=\"viewport\" "
                    "content=\"width=device-width, initial-scale=1\">");
  webClient.println("<link rel=\"icon\" href=\"data:,\">");
  // CSS to style the on/off buttons
  // Feel free to change the background-color and font-size attributes
  // to fit your preferences
  webClient.println("<style>html {"
                    "font-family: Helvetica;"
                    "display: inline-block;"
                    "margin: 0px auto;"
                    "text-align: center;"
                    "}");
  webClient.println(".button { background-color: #4CAF50; "
                    "border: none; "
                    "color: white; "
                    "padding: 16px 40px;");
  webClient.println("text-decoration: none;"
                    " font-size: 30px;"
                    " margin: 2px; "
                    "cursor: pointer;"
                    "}");
  webClient.println(".button2 {background-color: #555555;}</style></head>");

  // Web Page Heading
  webClient.println("<body><h1>Poolheizungssteuerung</h1>");

  const String roofTempStr(roofTemp, 1);
  const String poolTempStr(poolTemp, 1);
  const String tempGoalStr(tempGoal, 1);

  // Display current Temperatures
  webClient.println("<p>Temperatur auf dem Dach:\t\t" + roofTempStr + " " +
                    degree + "C</p>");
  webClient.println("<p>Pooltemperatur:\t\t" + poolTempStr + " " + degree +
                    "C</p>");
  webClient.println("<p>Solltemperatur:\t\t " + tempGoalStr + " " + degree +
                    "C</p>");
  webClient.println("<p><a href=\"/tempGoal/inc\"><button "
                    "class=\"button button2\">+</button></a>"
                    "<a href=\"/tempGoal/dec\"><button "
                    "class=\"button button2\">-</button></a></p>");
  // Display current state, and ON/OFF buttons for relay control
  webClient.println("<p>Heizung ist " + heatingState + "</p>");
  if (!manualOverride)
    webClient.println("<p><a href=\"/toggleManualOverride/on\"><button "
                      "class=\"button\">Manuelle Steuerung<br />"
                      "aktivieren</button></a></p>");
  if (manualOverride)
  {
    webClient.println("<p><a href=\"/toggleManualOverride/off\"><button "
                      "class=\"button "
                      "button2\">Manuelle Steuerung <br />"
                      "deaktivieren</button></a></p>");
    // If the output26State is off, it displays the ON button
    if (heatingState == "aus")
    {
      webClient.println("<p><a href=\"/relayManual/on\"><button "
                        "class=\"button\">Manuell <br />"
                        "Anschalten</button></a></p>");
    }
    else
    {
      webClient.println("<p><a href=\"/relayManual/off\"><button "
                        "class=\"button "
                        "button2\">Manuell <br />"
                        "Ausschalten</button></a></p>");
    }
  }
  webClient.println("</body></html>");

  // The HTTP response ends with another blank line
  webClient.println();
}
void readTemps(float &roofTemp, float &poolTemp)
{
  sensors.requestTemperatures();
  roofTemp = sensors.getTempC(roofTempSensor);
  poolTemp = sensors.getTempC(poolTempSensor);
}
void scanForWifiNetworks()
{ // WiFi.scanNetworks will return the number of
  // networks found
  Serial.println("scan start");
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
  {
    Serial.println("no networks found");
  }
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
}
void initWiFi()
{
  WiFi.mode(WIFI_MODE_APSTA);
  WiFi.softAP("ESP_Pool", "sooosecret");
  WiFi.disconnect();
  delay(100);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print('.');
    delay(1000);
  }
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  Serial.println(WiFi.localIP());
  Serial.print("RRSI (signal strength): ");
  Serial.println(WiFi.RSSI());
  Serial.println("Setup done");
}

void openValve()
{
  if (heatingState != "an")
  {
    digitalWrite(relayControlPinClose, HIGH);
    digitalWrite(relayControlPinOpen, LOW);
    lastOpenCloseMovement = millis();
    heatingState = "an";
    energyState = running;
    Serial.println("Opening");
  }
}

void closeValve()
{
  if (heatingState != "aus")
  {
    digitalWrite(relayControlPinOpen, HIGH);
    digitalWrite(relayControlPinClose, LOW);
    lastOpenCloseMovement = millis();
    Serial.println("Closing");
    heatingState = "aus";
    energyState = running;
  }
}

void cutEnergy()
{
  digitalWrite(relayControlPinOpen, HIGH);
  digitalWrite(relayControlPinClose, HIGH);
  heatingState = "aus";
  energyState = cut;
  Serial.println("Cut energy");
}