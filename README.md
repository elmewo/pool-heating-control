# Pool Heating Control

ESP32-WROOM-based control for pool heating.

Controls a 230V switch based on the temperature difference between roof-mounted heating pipes and pool water.